# TxCreate


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_from** | **str** | The source UTXO or account ID for the originating funds | 
**to** | [**[TxDestination]**](TxDestination.md) | A list of recipients | 
**index** | **int** | The UTXO index or the account Nonce | 
**fee** | **str** | The fee you are willing to pay (required only for Ethereum) for the transaction | [optional] 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


